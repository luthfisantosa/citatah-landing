<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'citatah_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RUG2]* f/mnK8$55Ck]DHfAwRbwsr3W_h}{a*e~}JYY]DmXtpF[z~lpTbjVunXg6' );
define( 'SECURE_AUTH_KEY',  '|&|{34kK/hS|C8%+[}x[LRV:EY_+<BWb;/ tK_:EWB;-^X#X$_bZ74Fqrzg6-1gZ' );
define( 'LOGGED_IN_KEY',    'Al?}vAV^Adhv;-KFMot[)$<1Xl=vG)*tIS=~_QdbSFNd2lkLP<Au=ZDl$DaQt>@g' );
define( 'NONCE_KEY',        '6d<@PU7.i6Xx3<1,qroEZ[e%;K5cV{#iU_?c1i1r*Z>nQ5yT& cc(t`rrI~Sa7a]' );
define( 'AUTH_SALT',        'M{#;<RS7SO}-jbEMy)wLt+[::+Z!1rG ?I~1j}/w_jPmye;z/S{# iVa bwSjwV2' );
define( 'SECURE_AUTH_SALT', 'A}_j8a1n%c[K0N[M-byrk?&rgX].DtQ.JlOTw+dMhEOe[sQM<kZ?Gc;5M:IjbCT&' );
define( 'LOGGED_IN_SALT',   'xw*{=Ud(UFW>C4aBe+RaX_)C|E;8s |<IV-R<,yI/r.C]O?izo:&,%Rh[0nFru$,' );
define( 'NONCE_SALT',       ' ??|VnyL^499h6N<;kirQjn^16kh-<6?a[q!=j]_;S-e>(-`)GAhy~mt17!{6m&F' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
