=== Urna ===

Contributors: automattic
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.0
Tested up to: 4.2.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called Urna, or underscores.

== Description ==

Urna is a flexible and customizable WooCommerce Multi-Store WordPress Theme that installs and changes any item in a matter of minutes via Powerful Theme Options, you can also customize Google fonts. No code is easy and simple.


== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Urna includes support for Infinite Scroll in Jetpack.

== Changelog ==

Version 1.0.0 - June 10, 2019
- Initial release

Version 1.0.1 - June 19, 2019
- Fix bug List View in Mobile
- Add new demo: Bike, Marketplace-v1

Version 1.0.2 - July 11, 2019
- [HOT] Compatible with Dokan and WC Marketplace.
- [ADD NEW] Add new 2 skins: Watch, Marketplace-v2
- [ADD NEW] Add new options "Show Sidebar Tabs" in Theme Options/Mobile/Mobile WooCommerce
- [ADD NEW] Add new options "Search In: Only Title/All (Title, Content, Sku)" in Theme Options/Header/Search Form
- [FIX] Fix bug Click "+" && "-" to work incorrectly at the page cart.
- [FIX] Fix Widget Urna Banner
- [UPDATE] Update documentation for Woocommerce and Multivendor.

Version 1.0.3 - July 25, 2019
- [ADD NEW] Add new 2 skins: Organic, Women
- [ADD NEW] Add new Revslider sample data of all skins compatible with Revslider 6.x
- [ADD NEW] Add new options "Enable Buy Now" in Theme Options/WooCommerce/Single Product Advanced Options
- [ADD NEW] Add new options "Enable Total Sales" in Theme Options/WooCommerce/Single Product Advanced Options
- [ADD] Add new option Custom Html "HTML before Add To Cart button (Global)" in Theme Options/WooCommerce/Single Product Advanced Options
- [ADD] Add new option Custom Html "HTML after Add To Cart button (Global)" in Theme Options/WooCommerce/Single Product Advanced Options
- Compatible with Woocommerce 3.6.5
- Compatible with WPBakery Page Builder 6.0.5
- Compatible with Slider Revolution Responsive 6.0.7
- [FIX] Fix bug search "All (Title, Content, Sku)" works incorrectly.
- [FIX] Fix bug can't translate text "Validate Registration" Form.

Version 1.0.4 - August 22, 2019
- Compatible with Woocommerce 3.7
- Compatible with WPBakery Page Builder 6.0.5
- Compatible with Slider Revolution Responsive 6.0.9
- [ADD NEW] Add new 2 skins: Toy, Glass
- [ADD NEW] Add new the option "Customize Photo Reviews Thumbnail Size"

Version 1.0.5 - August 23, 2019
- [HOT] Compatible with WCFM Marketplace.
- [ADD NEW] Add a new skin: Auto-part
- [FIX] Fix ajax load categories of add on "Products menu banner" on the skin "Book"


== Credits ==

* Based on Underscores http://underscores.me/,(C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/,(C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
